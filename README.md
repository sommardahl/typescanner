TypeScanner
===========



A simple c# type scanner that scans all local assemblies for implementations of a given type.

Installation
------------

http://nuget.org/packages/TypeScanner

```
nuget> install-package TypeScanner
```