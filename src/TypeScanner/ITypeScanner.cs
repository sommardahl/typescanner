﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace TypeScanner
{
    public interface ITypeScanner
    {
        List<Type> GetTypesOf(Type baseType);
        List<Type> GetTypesOf<TBaseType>();
    }
}