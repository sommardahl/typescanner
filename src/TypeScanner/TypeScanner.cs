﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace TypeScanner
{
    public class TypeScanner : ITypeScanner
    {
        readonly IEnumerable<Assembly> _assemblies;

        public TypeScanner(IEnumerable<Assembly> assemblies = null)
        {
            _assemblies = assemblies?? GetLocalAssemblies();
        }
        
        public List<Type> GetTypesOf<T>()
        {
            return GetTypesOf(typeof(T));
        }

        public List<Type> GetTypesOf(Type baseType)
        {
            var manyTypes = _assemblies
                .SelectMany(x => x.GetTypes());

            return manyTypes
                .Where(x => baseType.IsAssignableFrom(x)
                            && x.IsClass)
                .Where(x => x != baseType)
                .ToList();
        }

        static IEnumerable<Assembly> GetLocalAssemblies()
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies().Where(x => !x.IsDynamic).ToList();
            return assemblies;
        }
    }
}